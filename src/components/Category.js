import React, { Component } from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import Clue from './Clue'


export class Category extends Component{
    constructor(){
        super();

        this.state={clues: []};
    }

    componentDidMount(){
        fetch(`http://jservice.io/api/clues?category=${this.props.category.id}`)
        .then(response => response.json())
        .then(json => this.setState({clues: json}));//console.log(json) to check to make sure the data is pulling properly

    }
    render(){
        //console.log('category props', this.props);//category props updates with and object without the data 
        return(
            <div>
                <h2>{this.props.category.title}</h2>
                {
                    this.state.clues.map(clue =>{
                        return(
                            <Clue key={clue.id} clue ={clue}/>//{clue.question}</div> this part is removed because the clue component has been import with the question
                        )
                    })
                } 
            </div>
        )
    }
}
export class LinkedCategory extends Component{
    render(){
        return(
            <div>
                <Link className= 'link-home' to= '/'><h4>Home</h4></Link>
                <Category category={this.props.category}/>
            </div>
        )
    }
}

function mapStateToProps(state){
    return{category: state.category}
}

export default connect(mapStateToProps, null)(LinkedCategory);